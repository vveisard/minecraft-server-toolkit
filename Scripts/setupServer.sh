#!/bin/bash
# setup server with a modpack
# the modpack must already be downloaded
# args:
#	$1: server name
#	$2: id of the modpack that was download
#	$3: version of the modpack that was downloaded

# create server folder
sudo -u minecraft mkdir /home/minecraft/servers/$1

# download install forge
sudo -u minecraft wget https://files.minecraftforge.net/maven/net/minecraftforge/forge/1.15.2-31.1.18/forge-1.15.2-31.1.18-installer.jar /home/minecraft/forgeInstaller.jar
sudo -u minecraft java -jar /home/minecraft/installer.jar --installServer # do the installation

# install modpack
cd /home/minecraft/servers/$1
sudo -u minecraft python3.4 /home/minecraft/curseDownloader/downloader.py --manifest /home/minecraft/modpacks/$2/$3/manifest.json