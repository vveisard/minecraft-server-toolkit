#!/bin/bash
# download a modpack
# args 
#	$1: <url>
#	$2: <id>
#	$3: <version>

# make the directory for the mod
sudo -u minecraft mkdir /home/minecraft/modpacks/$2
sudo -u minecraft mkdir /home/minecraft/modpacks/$2/$3

# download the modpack zip
sudo -u minecraft wget $1 -O /home/minecraft/modpacks/$2/$3/src.zip

# unzip the modpack
sudo -u minecraft unzip /home/minecraft/modpacks/$2/$3/src.zip -d /home/minecraft/modpacks/$2/$3