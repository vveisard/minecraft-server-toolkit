git clone this repo to your machine

# New Setup
# setup the machine
$ setupMachine.sh

# Modpack Install
# download a modpack
$ downloadModpack.sh <modpackUrl> <modpackId> <modpackVersion>

# Server Setup
$ setupServer.sh <serverId> <modpackId> <modpackVersion>

# run the server2
$ screen /usr/bin/java -jar ~/servers/<serverId>/??????